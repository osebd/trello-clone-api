FROM python:3

RUN mkdir -p /trello-api

COPY . /trello-api

WORKDIR /trello-api

RUN pip3 install -r requirements.txt
