from rest_framework import serializers
from .models import Board
from trello.apps.list.serializers import ListSerializer

class BoardSerializer(serializers.ModelSerializer):
    lists = ListSerializer(source='list_set', many=True)
    class Meta:
        model = Board
        fields = ('id', 'title', 'lists')
        depth=2
