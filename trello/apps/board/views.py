import json
from django.http import HttpResponse, JsonResponse
from .serializers import BoardSerializer
from .models import Board
from django.views.decorators.csrf import csrf_exempt

# Create your views here.

def get_boards(request):
    response = BoardSerializer(Board.objects.all(), many=True).data

    return JsonResponse(json.dumps(response), safe=False)

@csrf_exempt
def create_board(request):
    data = json.loads(request.body)
    board_title = data['boardTitle']
    newBoard = Board.objects.create(title = board_title)

    return get_boards(request)

@csrf_exempt
def delete_board(request):
    data = json.loads(request.body)
    board_id = data['boardId']
    objects = Board.objects.all()
    Board.objects.filter(id = board_id).delete()

    return get_boards(request)

@csrf_exempt
def edit_board(request):
    data = json.loads(request.body)
    board_id = data['boardId']
    board_title = data['boardTitle']
    Board.objects.filter(id=board_id).update(title=board_title)

    return get_boards(request)


