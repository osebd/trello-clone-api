from django.urls import path
from . import views

urlpatterns = [
    path('get_boards', views.get_boards, name='index'),
    path('create_board', views.create_board, name='create_board'),
    path('delete_board', views.delete_board, name='delete_board'),
    path('edit_board', views.edit_board, name='edit_board'),
]

