from trello.apps.card.models import Card
from trello.apps.list.models import List
from django.views.decorators.csrf import csrf_exempt
import json
from trello.apps.board.views import get_boards
# Create your views here.


@csrf_exempt
def create_card(request):
    data = json.loads(request.body)
    card_content = data['cardContent']
    list_id = data['listId']
    belongs_to = List.objects.get(id=list_id)
    newCard = Card.objects.create(content = card_content, list = belongs_to)

    return get_boards(request)

@csrf_exempt
def delete_card(request):
    data = json.loads(request.body)
    card_id = data['cardId']
    Card.objects.filter(id = card_id).delete()

    return get_boards(request)


@csrf_exempt
def edit_card(request):
    data = json.loads(request.body)
    card_id = data['cardId']
    card_content = data['cardContent']
    Card.objects.filter(id=card_id).update(content=card_content)

    return get_boards(request)