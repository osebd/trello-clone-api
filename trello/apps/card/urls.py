from django.urls import path
from . import views

urlpatterns = [
    path('create_card', views.create_card, name='create_card'),
    path('edit_card', views.edit_card, name='edit_card'),
    path('delete_card', views.delete_card, name='delete_card'),
]