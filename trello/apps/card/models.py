from django.db import models
from trello.apps.list.models import List
# Create your models here.

class Card(models.Model):
    id = models.AutoField(primary_key=True, )
    content = models.TextField(null=False, blank=False, default='Content')
    list = models.ForeignKey(List, on_delete=models.CASCADE, null=True)
