from django.urls import path
from . import views

urlpatterns = [
    path('create_list', views.create_list, name='create_list'),
    path('edit_list', views.edit_list, name='edit_board'),
    path('delete_list', views.delete_list, name='delete_list'),
]
