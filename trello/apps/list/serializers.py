from rest_framework import serializers
from .models import List
from trello.apps.card.serializers import CardSerializer

class ListSerializer(serializers.ModelSerializer):
    cards = CardSerializer(many=True, read_only=True, source='card_set')
    class Meta:
        model = List
        fields = ('id', 'title', 'cards')

