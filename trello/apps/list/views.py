from trello.apps.board.models import Board
from trello.apps.list.models import List
from django.views.decorators.csrf import csrf_exempt
import json
from trello.apps.board.views import get_boards
# Create your views here.

@csrf_exempt
def create_list(request):
    data = json.loads(request.body)
    list_title = data['listTitle']
    belongs_to = Board.objects.get(id=data['boardId'])

    newList = List.objects.create(title = list_title, board=belongs_to)

    return get_boards(request)

@csrf_exempt
def delete_list(request):
    data = json.loads(request.body)
    list_id = data['listId']
    List.objects.filter(id = list_id).delete()

    return get_boards(request)

@csrf_exempt
def edit_list(request):
    data = json.loads(request.body)
    list_id = data['listId']
    list_title = data['listTitle']
    List.objects.filter(id=list_id).update(title=list_title)

    return get_boards(request)