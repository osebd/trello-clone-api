from django.db import models
from trello.apps.board.models import Board

# Create your models here.

class List(models.Model):
    id = models.AutoField(primary_key=True,)
    title = models.CharField(max_length=30, null=False, blank=False, default='Title')
    board = models.ForeignKey(Board, on_delete=models.CASCADE, null=True)